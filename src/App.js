import React, { Component } from "react";
import PageHeader from "./components/pageHeader";
import PageMain from "./components/pageMain";
import socialIcons from "./state/socialIcons";
import skillSets from "./state/skillSets";
import projects from "./state/projects";
import jobs from "./state/jobs";

class App extends Component {
  state = {
    socialIcons: socialIcons,
    skillSets: skillSets,
    projects: projects,
    jobs: jobs
  };

  render() {
    return (
      <article className="Page-content">
        <div className="Page-header">
          <PageHeader socialIcons={this.state.socialIcons} />
        </div>
        <div className="Page-main">
          <PageMain
            skillSets={this.state.skillSets}
            jobs={this.state.jobs}
            projects={this.state.projects}
          />
        </div>
      </article>
    );
  }
}

export default App;
