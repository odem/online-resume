const webDev = {
  id: 'skill-web-dev',
  category: 'Web Development',
  skills: [
    'HTML5',
    'CSS3',
    'Sass',
    'Bootstrap',
    'JavaScript',
    'jQuery',
    'ES6',
    'React',
    'Redux',
    'AJAX',
    'JSON',
    'PHP',
    'SQL',
    'Node',
  ],
};

const languages = {
  id: 'skill-prog-lang',
  category: 'Programming Languages',
  skills: ['Java', 'Python', 'C', 'Bash'],
};

const devTools = {
  id: 'skill-dev-tools',
  category: 'Development Tools',
  skills: [
    'Command Line Interface (CLI)',
    'Git',
    'SVN',
    'Gulp',
    'Mocha',
    'SSH',
    'Jira',
  ],
};

const editors = {
  id: 'skill-editors',
  category: 'IDE/Editors',
  skills: ['Visual Studio Code', 'Vim', 'Sublime'],
};

const operatingSystems = {
  id: 'skill-os',
  category: 'Operating Systems',
  skills: ['Macintosh', 'Linux', 'Windows'],
};

const other = {
  id: 'skill-other',
  category: 'Other',
  skills: ['Leadership', 'Project Management', 'Technical Writing', 'Research'],
};

const skillsets = [
  webDev,
  languages,
  devTools,
  editors,
  operatingSystems,
  other,
];

export default skillsets;
