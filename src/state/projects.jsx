const kanbanboard = {
  id: 'project-kanbanboard',
  title: 'KanbanBoard App (Work in Progress)',
  description:
    'This project is a simplified version of the KanbanFlow app, and is currently a work in progress. I created this project to expand my React.js and Redux.js skills. The UI components are mostly complete. The demo currently only has one default board with editable and movable cards. Currently, the cards cannot be moved on touch devices.',
  demo: {
    href: 'https://www.shanaodem.com/kanbanboard',
    linkTitle: 'View My KanbanBoard App.',
  },
  roles: [
    'Design',
    'User Experience (UX)',
    'Front-End Development',
    'Back-End Development',
  ],
  skills: [
    'React',
    'Redux',
    'Redux-Thunk',
    'SCSS',
    'Bootstrap',
    'JavaScript (ES6)',
    'Firebase',
    'Firestore',
  ],
};

const myportfolio = {
  id: 'project-myportfolio',
  title: 'My Portfolio Website',
  description:
    'This site highlights my professional career, and the advancement of my web development skills. The site is responsive with JavaScript fallbacks, accessibility features, and print stylings.',
  demo: {
    href: 'https://www.shanaodem.com',
    linkTitle: 'View My Portfolio Site.',
  },
  roles: [
    'Design',
    'User Experience (UX)',
    'Front-End Development',
    'Back-End Development',
  ],
  skills: ['HTML5', 'SCSS', 'JavaScript', 'PHP', 'Mocha', 'Gulp', 'Node'],
};

const whineycataudio = {
  id: 'project-whineycataudio',
  title: 'Whiney Cat Audio Website',
  description:
    'Whiney Cat Audio is a Seattle recording studio, whose aim is to highlight their services, studio space, and past work. The site is responsive with JavaScript fallbacks, accessibility features, and print stylings.',
  demo: {
    href: 'https://www.whineycataudio.com',
    linkTitle: 'View Whiney Cat Audio Website.',
  },
  roles: ['Co-Design', 'User Experience (UX)', 'Front-End Development'],
  skills: ['HTML5', 'CSS3', 'JavaScript'],
};

const brianpake = {
  id: 'project-brianpake',
  title: 'Brian Pake Portfolio Website',
  description:
    'Brian Pake is a Seattle audio engineer and musician, whose aim is to highlight his musical and engineering career by displaying his work through audio, video, and media press. The site is responsive with JavaScript fallbacks, accessibility features, and print stylings.',
  demo: {
    href: 'https://www.brianpake.com',
    linkTitle: "View Brian Pake's Portfolio Website.",
  },
  roles: ['Co-Design', 'User Experience (UX)', 'Front-End Development'],
  skills: ['HTML5', 'CSS3', 'JavaScript'],
};

const ggforces = {
  id: 'project-ggforces',
  title: 'GG Forces',
  description:
    'GG Forces is an international shipping logistics company, whose aim is to highlight their services, past clients, and business philosophy to better market themselves. GG Forces provided a site template from another developer, which I modified to meet their needs. I improved the responsiveness of the template, as well as added JavaScript fallbacks, and various accessibility features.',
  demo: {
    href: 'https://www.ggforces.com',
    linkTitle: "View GG Forces' Website.",
  },
  roles: ['Front-End Development'],
  skills: ['HTML5', 'SCSS', 'JavaScript', 'jQuery', 'Vim'],
};

const projects = [
  kanbanboard,
  myportfolio,
  whineycataudio,
  brianpake,
  ggforces,
];

export default projects;
