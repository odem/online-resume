const freelance = {
  id: 'job-freelance',
  position: 'Front-End Software Engineer',
  company: 'Freelance, Seattle, WA',
  dates: { start: 'Mar 2017', end: 'Present', dateStart: '2017-03' },
  duties: [
    {
      id: 1,
      duty:
        'Created responsive, accessible, and search engine optimized (SEO) websites for clients.',
    },
    {
      id: 2,
      duty:
        'Used React, Redux, and ES6 to develop reusable user interface (UI) components to build front-end applications.',
    },
    {
      id: 3,
      duty:
        'Created modular HTML5 templates using Handlebars.js, and decoupled HTML5, CSS3/Sass, and JavaScript using BEM.',
    },
    {
      id: 4,
      duty: 'Performed unit, cross-browser, and user interface testing.',
    },
    {
      id: 5,
      duty:
        'Used AJAX to transfer JSON data, and PHP and SQL to perform back-end functionality.',
    },
    {
      id: 6,
      duty:
        'Increased project management efficiency by 30% using Git, Gulp, Sass, Mocha, and Node.',
    },
  ],
};

const micron = {
  id: 'job-micron',
  position: 'DRAM Test Engineer',
  company: 'Micron Technology, Boise, ID',
  dates: {
    start: 'July 2016',
    end: 'Feb 2017',
    dateStart: '2016-07',
    dateEnd: '2017-02',
  },
  duties: [
    {
      id: 1,
      duty:
        'Maintained a version controlled automated test program using Python, C, and SVN, including test software releases.',
    },
    {
      id: 2,
      duty:
        'Demonstrated technical writing skills by collecting, analyzing, and documenting test results.',
    },
    {
      id: 3,
      duty:
        'Worked with a team to ensure test program efficiency, including receiving and providing performance feedback.',
    },
    {
      id: 4,
      duty:
        'Used project management skills and Jira to communicate issues after debugging.',
    },
  ],
};

const uwresearch = {
  id: 'job-uwresearch',
  position: 'Undergraduate Research Assistant',
  company: 'NOISE Lab, University of Washington, Seattle, WA',
  dates: {
    start: 'Sept 2015',
    end: 'June 2016',
    dateStart: '2015-09',
    dateEnd: '2016-06',
  },
  duties: [
    {
      id: 1,
      duty:
        'Assisted graduate students in researching 2D materials for nanophotonic devices, including reading and analyzing academic papers.',
    },
    {
      id: 2,
      duty: 'Assisted in creating nanophotonic LEDs and lasers.',
    },
    {
      id: 3,
      duty: 'Performed photoluminescence spectroscopy.',
    },
  ],
};

const ucberkeley = {
  id: 'job-ucberkeley',
  position: 'Lead Technology Help Desk Staff',
  company: 'University of California, Berkeley, CA',
  dates: {
    start: 'Aug 2008',
    end: 'Aug 2010',
    dateStart: '2008-08',
    dateEnd: '2010-08',
  },
  duties: [
    {
      id: 1,
      duty: 'Led a campus-wide help desk team of 3 staff and 10 students.',
    },
    {
      id: 2,
      duty:
        'Demonstrated leadership with training and supervising students, and supervising a department transition period.',
    },
    {
      id: 3,
      duty:
        'As a select member of a process-analysis team, collected and analyzed user needs and department resources to design and implement new procedures; new procedures resulted in a 50% increase in client satisfaction.',
    },
    {
      id: 4,
      duty:
        'Used project management skills and Jira to fix, triage, and communicate equipment issues and help desk operations.',
    },
    {
      id: 5,
      duty:
        'Demonstrated technical writing skills by creating department reports and user tutorials that adhered to the university guidelines.',
    },
  ],
  award: 'Campus Staff Spot Recognition (campus-wide staff award)',
};

const jobs = [freelance, micron, uwresearch, ucberkeley];

export default jobs;
