import React from "react";

const Skillset = props => {
  return (
    <dl className={["SkillSet", ...props.containerClasses].join(" ")}>
      <dt className={["Skillset-title", ...props.titleClasses].join(" ")}>
        {props.skillset.category}
      </dt>
      <dd>
        <ul>
          {props.skillset.skills
            // Adds comma after each skill except last skill.
            .join(",  ")
            .split("  ")
            .map(skill => (
              <li className="Skillset-skill" key={skill}>
                {skill}
              </li>
            ))}
        </ul>
      </dd>
    </dl>
  );
};

export default Skillset;
