import React from "react";
import SocialIcon from "./socialIcon";
import logo from "../icons/logo.png";

const PageHeader = props => {
  return (
    <header className="PageHeader">
      <div className="PageHeader-content">
        <img className="PageHeader-logo Page-img" src={logo} alt="Logo" />

        <div className="PageHeader-sxnTitle">
          <h1 className="PageHeader-title Page-title">Shana Odem</h1>

          <button
            className="Tooltip PageHeader-tooltip"
            aria-labelledby="pronounce-name"
          >
            <span
              className="Tooltip-text PageHeader-tooltipText"
              id="pronounce-name"
              role="tooltip"
            >
              Pronounced SHAY&#8231;nuh OH&#8231;dem
            </span>
          </button>
        </div>

        <h2 className="PageHeader-headline Page-headline">Software Engineer</h2>
        <p className="PageHeader-text">Online Resume</p>
      </div>

      <div className="PageHeader-socialFooter">
        <ul>
          {props.socialIcons.map(icon => (
            <li className="SocialIcon Page-socialIcon" key={icon.id}>
              <SocialIcon
                href={icon.linkHref}
                linkTitle={icon.linkTitle}
                linkClasses={["Page-link"]}
                imgSrc={icon.imgSrc}
                imgAlt={icon.imgAlt}
              />
            </li>
          ))}
        </ul>

        <p className="PageHeader-socialFooterText">Seattle, WA</p>
      </div>
    </header>
  );
};

export default PageHeader;
