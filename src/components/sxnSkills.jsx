import React from "react";
import Sxn from "./sxn";
import Skillset from "./skillset";

const SxnSkills = props => {
  return (
    <Sxn
      containerClasses={props.sxnClasses}
      titleClasses={props.sxnTitleClasses}
      title="Skills"
    >
      {props.skillSets.map(skillset => (
        <Skillset
          containerClasses={["SxnSkill-skillset"]}
          titleClasses={["Page-headlineSub"]}
          skillset={skillset}
          key={skillset.id}
        />
      ))}
    </Sxn>
  );
};

export default SxnSkills;
