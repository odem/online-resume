import React from "react";
import Sxn from "./sxn";

const SxnIntro = props => {
  return (
    <Sxn
      containerClasses={props.sxnClasses}
      titleClasses={props.sxnTitleClasses}
      title="Summary"
    >
      <p>
        I enjoy building and improving applications to have a strong user focus
        by blending my technical user support background with my programming and
        engineering skills. I am able to keep the big picture insight while
        digging through the details of implementation. I have worked in various
        fast-paced environments, and willingly taken on the challenge of
        learning something new outside my comfort zone. My top priorities are
        maintainability, scalability, efficiency, and user experience.
      </p>
    </Sxn>
  );
};

export default SxnIntro;
