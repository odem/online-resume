import React from "react";

const Sxn = props => {
  return (
    <section className={["Sxn", ...props.containerClasses].join(" ")}>
      <h2 className={["Sxn-title", ...props.titleClasses].join(" ")}>
        {props.title}
      </h2>
      <div className="Sxn-content">{props.children}</div>
    </section>
  );
};

export default Sxn;
