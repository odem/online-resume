import React from "react";

const ListSingleLine = props => {
  return (
    <dl className="ListSingleLine">
      <dt className={["ListSingleLine-title", ...props.titleClasses].join(" ")}>
        {props.title}
      </dt>
      <dd className="ListSingleLine-content">
        <ul className="ListSingleLine-items">
          {props.items
            // Adds comma after each item except for last item.
            .join(",  ")
            .split("  ")
            .map(item => (
              <li className="ListSingleLine-item" key={item}>
                {item}
              </li>
            ))}
        </ul>
      </dd>
    </dl>
  );
};

export default ListSingleLine;
