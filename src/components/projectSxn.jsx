import React from "react";
import ListSingleLine from "./listSingleLine";

const ProjectSxn = props => {
  return (
    <section className={props.containerClasses.join(" ")}>
      <h3 className={props.titleClasses.join(" ")}>
        <a
          className="Page-link"
          href={props.project.demo.href}
          title={props.project.demo.linkTitle}
          target="_blank"
          rel="noopener noreferrer"
        >
          {props.project.title}
        </a>
      </h3>
      <p className="ProjectSxn-description">{props.project.description}</p>
      <ListSingleLine
        titleClasses={["ProjectSxn-itemTitle"]}
        title="Roles:"
        items={props.project.roles}
      />
      <ListSingleLine
        titleClasses={["ProjectSxn-itemTitle"]}
        title="Skills:"
        items={props.project.skills}
      />
    </section>
  );
};

export default ProjectSxn;
