import React from "react";
import Sxn from "./sxn";
import ProjectSxn from "./projectSxn";

const SxnProjects = props => {
  return (
    <Sxn
      containerClasses={props.sxnClasses}
      titleClasses={props.sxnTitleClasses}
      title="Projects"
    >
      {props.projects.map(project => (
        <ProjectSxn
          containerClasses={["SxnProject-sxn"]}
          titleClasses={["Sxn-headline", "Page-headlineSub"]}
          key={project.id}
          project={project}
        />
      ))}
    </Sxn>
  );
};

export default SxnProjects;
