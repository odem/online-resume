import React from "react";
import Sxn from "./sxn";

const SxnEducation = props => {
  return (
    <Sxn
      containerClasses={props.sxnClasses}
      titleClasses={props.sxnTitleClasses}
      title="Education"
    >
      <h3 className="Sxn-headline Page-headlineSub">
        University of Washington, Seattle, WA
      </h3>
      <p>Bachelor of Science in Electrical Engineering (BSEE)</p>
      <time className="Sxn-date" dateTime="2016-06">
        June 2016
      </time>
      <p className="Sxn-textHighlight">
        Award: Outstanding Electrical Engineering Undergraduate Student
        (department award)
      </p>
      <p>
        Related Courses: Computer Programming I &amp; II, Intermediate Computer
        Programming, Data Structures and Algorithms
      </p>
    </Sxn>
  );
};

export default SxnEducation;
