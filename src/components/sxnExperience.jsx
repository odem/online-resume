import React from "react";
import Sxn from "./sxn";
import ExperienceSxn from "./experienceSxn";

const SxnExperience = props => {
  return (
    <Sxn
      containerClasses={props.sxnClasses}
      titleClasses={props.sxnTitleClasses}
      title="Experience"
    >
      {props.jobs.map(job => (
        <ExperienceSxn
          containerClasses={["SxnExperience-sxn"]}
          headlineClasses={["Sxn-headline", "Page-headlineSub"]}
          highlightClasses={["Sxn-textHighlight"]}
          dateClasses={["Sxn-date"]}
          job={job}
          key={job.id}
        />
      ))}
    </Sxn>
  );
};

export default SxnExperience;
