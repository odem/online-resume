import React from "react";

function getAward(award, classes) {
  if (award)
    return (
      <p className={classes}>
        <span>Award: </span>
        {award}
      </p>
    );
}

function getDate(date, dateDisplay) {
  let time;

  if (dateDisplay) {
    if (date) {
      time = <time dateTime={date}>{dateDisplay}</time>;
    } else {
      time = dateDisplay;
    }
  }

  return time;
}

const ExperienceSxn = props => {
  return (
    <section className={["ExperienceSxn", ...props.containerClasses].join(" ")}>
      <header className="ExperienceSxn-header">
        <h3 className={props.headlineClasses.join(" ")}>
          {props.job.position}
        </h3>
        <h4 className={"ExperienceSxn-headline"}>{props.job.company}</h4>
        <div className={props.dateClasses.join(" ")}>
          {getDate(props.job.dates.dateStart, props.job.dates.start)}
          {" - "}
          {getDate(props.job.dates.dateEnd, props.job.dates.end)}
        </div>
        {getAward(
          props.job.award,
          ["ExperienceSxn-headline", ...props.highlightClasses].join(" ")
        )}
      </header>

      <ul className="ExperienceSxn-duties">
        {props.job.duties.map(duty => (
          <li key={duty.id}>{duty.duty}</li>
        ))}
      </ul>
    </section>
  );
};

export default ExperienceSxn;
