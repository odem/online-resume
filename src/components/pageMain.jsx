import React from "react";
import SxnIntro from "./sxnIntro";
import SxnSkills from "./sxnSkills";
import SxnExperience from "./sxnExperience";
import SxnProjects from "./sxnProjects";
import SxnEducation from "./sxnEducation";

const PageMain = props => {
  return (
    <main className="PageMain">
      <SxnIntro
        sxnClasses={["PageMain-sxn"]}
        sxnTitleClasses={["Page-headline"]}
      />
      <SxnSkills
        sxnClasses={["PageMain-sxn"]}
        sxnTitleClasses={["Page-headline"]}
        skillSets={props.skillSets}
      />
      <SxnExperience
        sxnClasses={["PageMain-sxn"]}
        sxnTitleClasses={["Page-headline"]}
        jobs={props.jobs}
      />
      <SxnProjects
        sxnClasses={["PageMain-sxn"]}
        sxnTitleClasses={["Page-headline"]}
        projects={props.projects}
      />
      <SxnEducation
        sxnClasses={["PageMain-sxn"]}
        sxnTitleClasses={["Page-headline"]}
      />
      <div className="ResumeBtn">
        <a
          className="Btn Btn--themeOutline"
          href="https://www.shanaodem.com/resume.html"
          target="_blank"
          rel="noopener noreferrer"
          aria-disabled="false"
        >
          Get My Resume
        </a>
      </div>
    </main>
  );
};

export default PageMain;
