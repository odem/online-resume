import React from "react";

function getTargetAttr(link) {
  if (!link.includes("mailto")) {
    return "_blank";
  }
}

function getRelAttr(link) {
  if (!link.includes("mailto")) {
    return "noopener noreferrer";
  }
}

const SocialIcon = props => {
  return (
    <a
      className={["SocialIcon-link", ...props.linkClasses].join(" ")}
      href={props.href}
      target={getTargetAttr(props.href)}
      rel={getRelAttr(props.href)}
      title={props.linkTitle}
    >
      <img className="SocialIcon-img" src={props.imgSrc} alt={props.imgAlt} />
    </a>
  );
};

export default SocialIcon;
