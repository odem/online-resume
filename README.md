# Online Resume

This project was created to learn the basics of React.js, including JSX and pure react, class-based and stateless functional components, state and props, one source of truth, and other fundamentals. This project is responsive with accessibility features and print stylings. Since server-side rendering is not setup for this project, a JavaScript fallback page was created.

September 2018

## Demo

https://www.shanaodem.com/online_resume/

## My Roles

- Design
- User-Experience
- Front-End Development

## Tools/Skills Used

- HTML5
- CSS3
- JavaScript (ES6)
- React.js
