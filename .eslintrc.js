module.exports = {
  env: {
    es6: true
  },
  extends: "react-app",
  parserOptions: {
    ecmaVersion: 6,
    sourceType: "module",
    ecmaFeatures: {
      jsx: true
    }
  },
  rules: {
    "no-mixed-spaces-and-tabs": "error",
    "no-tabs": "error",
    "no-use-before-define": "warn",
    "no-var": "warn"
  }
};
